package com.sd.b1g4;

import com.sd.b1g4.MyDatabaseCursorActivity2.DatabaseHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
 
public class exerView extends Activity {
    //TextView text01;
	
	
	
    SQLiteDatabase database;
    String tableName = "PersonRecord"; 
    DatabaseHelper helper;
    /*
    EditText editName;
    EditText editSex;
    EditText editAge;
    EditText editWeight;
    EditText editHeight;
   */ 
    ListView list03;
  
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exer_main);
 
       
        //text01 = (TextView) findViewById(R.id.text01);     
        list03 = (ListView) findViewById(R.id.list03);
        /*
        editName= (EditText)this.findViewById(R.id.editName);
        editSex= (EditText)this.findViewById(R.id.editSex);
        editAge= (EditText)this.findViewById(R.id.editAge);
        editWeight= (EditText)this.findViewById(R.id.editWeight);
        editHeight= (EditText)this.findViewById(R.id.editHeight);
               
        //DB만들기 버튼
        Button button01 = (Button) findViewById(R.id.button01);
        button01.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                createDatabase();
            }
        });
 */ 
        //데이터 조회
        Button bt_view2 = (Button) findViewById(R.id.bt_view2);
        bt_view2.setOnClickListener(new OnClickListener() {
        	 public void onClick(View v) {
                 Cursor cursor = queryData();
                 //queryDataParam();
                 if(cursor != null){
                     startManagingCursor(cursor); //액티비티가 커서를 관리
                     String[] columns = {"_id","distance","time","calory","date"};
                     int [] resIds = {R.id.text01,R.id.text02,R.id.text03,R.id.text04,R.id.text05};
                     //리턴된 커서의 컬럼과 listitem.xml에서 준비된 리소스아이디와 연결(매칭)
                    
                     //public SimpleCursorAdapter (Context context, int layout, Cursor c, String[] from, int[] to)
                     SimpleCursorAdapter adapter = new SimpleCursorAdapter(getApplicationContext(),R.layout.listitem,cursor, columns,resIds);
                    
                     list03.setAdapter(adapter); //리스트에 아답터 부착
                     //_id 필드명 필요 
                    
                 }//end if    
            }           
        });    
        
        Button bt_edit2 = (Button) findViewById(R.id.bt_edit2);
        bt_edit2.setOnClickListener(new OnClickListener(){
        	public void onClick (View v){
        		Intent intent = new Intent(exerView.this, MyDatabaseCursorActivity2.class);
        		startActivity(intent);
        		finish();
        	}
        });
        
        Button bt_delete = (Button) findViewById(R.id.bt_delete);
        bt_delete.setOnClickListener(new OnClickListener(){
        	public void onClick (View v){
        		 deleteDatabase();
        		 
        		 Cursor cursor = queryData();
                 //queryDataParam();
                 if(cursor != null){
                     startManagingCursor(cursor); //액티비티가 커서를 관리
                     String[] columns = {"_id","distance","time","calory","date"};
                     int [] resIds = {R.id.text01,R.id.text02,R.id.text03,R.id.text04,R.id.text05};
                     //리턴된 커서의 컬럼과 listitem.xml에서 준비된 리소스아이디와 연결(매칭)
                    
                     //public SimpleCursorAdapter (Context context, int layout, Cursor c, String[] from, int[] to)
                     SimpleCursorAdapter adapter = new SimpleCursorAdapter(getApplicationContext(),R.layout.listitem,cursor, columns,resIds);
                    
                     list03.setAdapter(adapter); //리스트에 아답터 부착
                     //_id 필드명 필요 
                    
                 }//end if    
        		 
        		 
        		
        	}
        });
        
        
    }
    //table 삭제
    private void deleteDatabase(){
        String name = "DB_Record.db";
        //database = openOrCreateDatabase(name, MODE_WORLD_WRITEABLE, null); //DB가 존재하면 오픈. 존재하지않은면 생성
        //int version = 1;
        int version = 1; //Helper의 onUpgrade메소드 호출 확인
        helper = new DatabaseHelper(this, name, null, version);
        database = helper.getWritableDatabase(); //DB에 참조하여 읽거나 쓸수있다
        helper.deleteTable(database);
        }
   
   
   
    //데이터 조회
    private Cursor queryData(){
    	 String name = "DB_Record.db";
    	 int version =1;
    	 helper = new DatabaseHelper(this, name, null, version);
         database = helper.getReadableDatabase(); //DB에 참조하여 읽거나 쓸수있다
    	
    	
        String sql = "select _id,distance,time,calory,date from "+ tableName + "";
        Cursor cursor = database.rawQuery(sql, null);

        if(cursor != null){
            int count = cursor.getCount(); //조회된 개수얻기
            //text01.append("헬퍼안에서 데이터를 조회했어요. 레코드 갯수: "+count + "\n");
        }
       
        return cursor;       
    }
   
   
    private void queryDataParam(){
   
       
        //String sql = "select id,name,age from "+ tableName + " where age > 20";
        String[] columns = {"_id","distance", "time", "calory","date"}; //추출할 필드명
        String selection = "calory > ?"; //검색 조건
        String[] selectionArgs = {"1"};  //?를 대체할 값
       
        //Cursor cursor = database.query(tableName, columns, selection, selectionArgs, groupBy, having, orderBy)
        Cursor cursor = database.query(tableName, columns, selection, selectionArgs, null, null, null);
                               
       
        if(cursor != null){
            int count = cursor.getCount(); //조회된 개수얻기
           // text01.append("헬퍼안에서 데이터를 조회했어요. 레코드 갯수: "+count + "\n");
           
            for(int i = 0; i< count ; i++){
                cursor.moveToNext();
               
                String name=cursor.getString(0) + "/" +cursor.getString(1) +"/"+ cursor.getString(2);
               // text01.append("데이터 #"+i+":"+name+"\n");
            }
        }  
       
    }//queryDataParam
   
   
   
   
    //SQLiteOpenHelper클래스를 상속받은  DatabaseHelper이너 클래스 작성
    class DatabaseHelper extends SQLiteOpenHelper{
       
        //생성자
        public DatabaseHelper(Context context, String name,
                CursorFactory factory, int version) {
            super(context, name, factory, version);
        }
 
       
        @Override //추상클래스 구현
        public void onCreate(SQLiteDatabase db) {
           
            //text01.append("헬퍼를 이용해서 데이터베이스가 만들어졌어요\n");
            createTable(db);
            //insertData(db);
        }
 
        @Override //추상클래스 구현
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
           // text01.append("헬퍼를 이용해서 데이터베이스를 업그레이드 했어요 -> 이전버전 : "+oldVersion+", 현재버전 : "+newVersion+"\n");
 
            //테이블을 변경하고자할때...
        }
       
        @Override
        public void onOpen(SQLiteDatabase db) {
            //text01.append("헬퍼를 이용해서 데이터베이스를 오픈했어요.\n");
 
            super.onOpen(db);
           
        }
        //data삭제
        private void deleteTable(SQLiteDatabase db) {
            

       	 String sql = "DELETE FROM " +tableName+";";
       	 db.execSQL(sql);
        }
       
        //테이블 생성
        private void createTable(SQLiteDatabase db) {
           
           
            String sql = "create table " + tableName + "(_id INTEGER PRIMARY KEY AUTOINCREMENT,distance integer,time integer, calory integer,date integer)";
           
            try {
            	
                db.execSQL(sql);//slq문 실행
               // text01.append("헬퍼안에서 테이블이 만들어졌어요"+tableName+"\n");
            } catch (Exception e) {
               // text01.append("테이블 만들 때 예외 : "+e.getMessage()+"\n");
                e.printStackTrace();
            }
 
        }
        


		private EditText findViewById(int editname) {
			// TODO Auto-generated method stub
			return null;
		}
       
    }//end innerClass DatabaseHelper
 
}//end

