package com.sd.b1g4;

/**
 * 
 * @author Kim yoon kyoung
 * @version 1.0
 * @since 13.11.08
 * 
 * @ TimeCommand class
 * 
 */
public class TimeCommand implements AlarmCommand{
	
	TimeAlarmC command;
	
	public TimeCommand(TimeAlarmC command){
		
		this.command = command;
	}
	
	
	@Override
	public int execute() {
		return command.timecontrol();
	}
	
	@Override
	public void execAlarm(){
		command.exacAlarm();
	}


}
