//                ********************3초간 splash 보여주는 클래스*******************
package com.sd.b1g4;

import android.app.Activity;
import android.os.*;
import android.view.Window;
import android.content.Intent;

public class SplashActivity extends Activity {
	
	Handler h;
	@Override
	protected void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		h = new Handler();
		h.postDelayed(initialize, 3000);    //3초간 보여준다.
	}
	
	Runnable initialize = new Runnable() {
		public void run() {
			Intent i = new Intent(SplashActivity.this, StartActivity.class);   //다 보여주면, start 클래스로
			startActivity(i);
			finish();
			overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		}
	};
	
	@Override     //뒤로가기 누르면 끝
	public void onBackPressed(){
		super.onBackPressed();
		h.removeCallbacks(initialize);
	}
	
}
