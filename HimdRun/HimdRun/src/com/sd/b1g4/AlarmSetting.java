//                   ********************알람 셋팅 하는 class*******************
//                   Start Activity 로 부터 불린다.
//                   알람실행하기 위해 -> AlarmReceiver 연결
//                   뒤로가기 -> StartActivity

package com.sd.b1g4;

import java.util.Calendar;
import java.text.DateFormat;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.Menu;
import android.app.TimePickerDialog;
import android.widget.*;
import android.app.DatePickerDialog;


public class AlarmSetting extends Activity{
   
	@Override
	public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.alarmsetting);
        
        Button btn1 = (Button)findViewById(R.id.timeBtn);
        Button btn2 = (Button)findViewById(R.id.distanceBtn);
         
        
        /**
         * 시간알람버튼 눌렀을 때
         */
        btn1.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {    		
        		Intent intent = new Intent(AlarmSetting.this, TimeAlarmC.class);	
        		startActivity(intent);
        		finish();
			}
        });
        
        
        //distance alarm
        btn2.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		Intent intent = new Intent(AlarmSetting.this, DistanceAlarmC.class);     		
        		startActivity(intent);
        		finish();
			}
        });
	}
	
	
	@Override   //뒤로가기 버튼 누르면, 다시  start 화면으로~~
	public void onBackPressed(){
		super.onBackPressed();
		Intent intent = new Intent(AlarmSetting.this, StartActivity.class);	
		startActivity(intent);
		finish();
	}
}
