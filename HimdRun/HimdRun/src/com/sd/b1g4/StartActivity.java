//             ************�� ù��° ���� ȭ�� �� Ŭ���� **************
//             �˶���ư -> AlarmSetting
//             ����� -> MapActivity

package com.sd.b1g4;


import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;

public class StartActivity extends Activity  {
 
	Button set_Alarm;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //startActivity(new Intent(this, SplashActivity.class));    
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.start);
        
        //�˶���ư ������
        Button set_Alarm = (Button) findViewById(R.id.set_Alarm);
        set_Alarm.setOnClickListener(new OnClickListener(){
        	public void onClick (View v){
        		Intent intent = new Intent(StartActivity.this, AlarmSetting.class);
        		
        		startActivity(intent);
        		finish();
        	}
        });
               
        //����۹�ư ������
        Button set_Map = (Button) findViewById(R.id.set_Map);
        set_Map.setOnClickListener(new OnClickListener(){
        	public void onClick (View v){
        		Intent intent = new Intent(StartActivity.this, MapActivity.class);
        		startActivity(intent);
        		finish();
        	}
        });
       
        
        Button set_Info = (Button) findViewById(R.id.set_Info);
        set_Info.setOnClickListener(new OnClickListener(){
        	public void onClick (View v){
        		Intent intent = new Intent(StartActivity.this, infoView.class);
        		startActivity(intent);
        		finish();
        	}
        });
        
        Button set_record = (Button) findViewById(R.id.set_record);
        set_record.setOnClickListener(new OnClickListener(){
        	public void onClick (View v){
        		Intent intent = new Intent(StartActivity.this, exerView.class);
        		startActivity(intent);
        		finish();
        	}
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    

}
