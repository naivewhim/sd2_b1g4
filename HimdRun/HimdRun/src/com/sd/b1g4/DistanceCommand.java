package com.sd.b1g4;

/**
 * 
 * @author Choi do ill
 * @version 1.0
 * @since 13.11.09
 *
 * @ DistanceCommand class
 * 
 */
public class DistanceCommand implements AlarmCommand{
	
	DistanceAlarmC command;
	
	public DistanceCommand(DistanceAlarmC command) {
		
		this.command = command;
	}
	
	@Override
	public void execute() {
		
		command.dcal();
	}

}
