package com.sd.b1g4;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.view.*;
import android.content.*;

public class DistanceAlarmC extends Activity {
	
	//입력한 km,m
	String kmSet, mSet;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.disset);		
		
		Button btn = (Button)findViewById(R.id.saveBtn);  
		btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				EditText kmID = (EditText)findViewById(R.id.km);
				kmSet = kmID.getText().toString();
				
				EditText mID = (EditText)findViewById(R.id.miter);
				mSet = mID.getText().toString();
				
				Intent intent = new Intent(DistanceAlarmC.this, AlarmSetting.class);	
				startActivity(intent);
				finish();
    		
		}
    });
		
}
	
	public void dcal() {
		
	}
	
	@Override   //뒤로가기 버튼 누르면, 다시  start 화면으로~~
	public void onBackPressed(){
		super.onBackPressed();
		Intent intent = new Intent(DistanceAlarmC.this, AlarmSetting.class);	
		startActivity(intent);
		finish();
	}


}
