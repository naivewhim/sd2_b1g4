package com.sd.b1g4;

/**
 * @author Kim yoon kyoung
 * 
 * @since 13.11.15
 * @version 1.1
 * 
 * @ Calculate class
 *
 */

public interface Calculate {
	
	public double calculate();

}
