package com.sd.b1g4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;


 /**
  * 
 * @author Kim yoon kyoung
 * @version 1.0
 * @since 13.11.08
 * 
 * @ AlarmRemoteControl class
 *
 */
public class AlarmRemoteControl extends Activity{

	AlarmCommand slot;
	
	public AlarmRemoteControl() {}
	
	public void setCommand(AlarmCommand command) {
		slot=command;
	}
	
	public int startRunning() {
		return slot.execute();
	}
	
	public void startAlarm() {
		slot.execAlarm();
	}
	
	
	@Override   //뒤로가기 버튼 누르면, 다시  start 화면으로~~
	public void onBackPressed(){
		super.onBackPressed();
		Intent intent = new Intent(AlarmRemoteControl.this, StartActivity.class);	
		startActivity(intent);
		finish();
	}
	
	
	

}
