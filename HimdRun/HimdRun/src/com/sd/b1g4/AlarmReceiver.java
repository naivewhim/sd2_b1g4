//                    **********************알람리시버*********************
//                    intent 없고, MySoundPlay relation 으로 가져다 쓴다.

package com.sd.b1g4;

/**
 * 
 * @author Choi doo il
 * @version 1.0
 * @since 13.11.02
 * 
 * @ AlarmReceiver class
 * 
 */
 
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {

	private static MySoundPlay mplay = null;
	
	@Override
	public void onReceive(Context context, Intent intent){
		
		try {
			if(mplay == null) {
				mplay = new MySoundPlay(context, R.raw.dingdong);   //음악파일 이름삽입
			}
			mplay.play();
		}catch(Exception e) {}
		
		Toast.makeText(context, "alarm", Toast.LENGTH_LONG).show();  //알람이라고 창띄워주기
	}

}