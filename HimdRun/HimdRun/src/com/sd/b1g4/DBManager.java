package com.sd.b1g4;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/*
 * DB 생성 및 업그레이드를 도와주는 도우미 클래스 만들기
 *  - SQLiteOpenHelper 추상 클래스를 상속받아서 만든다. - 
 */
public class DBManager extends SQLiteOpenHelper {
	private static final String DB_NAME = "DB_Project.db";
	private static final int DB_version =1;
	String sql;
	public DBManager(Context context) {
		super(context, DB_NAME, null, DB_version);
		// TODO Auto-generated constructor stub
		
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		sql = "CREATE TABLE PersonInf ( _id INTEGER PRIMARY KEY AUTOINCREMENT," +
				   " name TEXT,"+ 
				 " age INTEGER,"+
				   " sex TEXT," +
				 " weight INTEGER," +
				   " height INTEGER);";
		  db.execSQL(sql);
		
		 sql = "CREATE TABLE ExRecord ( _id INTEGER PRIMARY KEY AUTOINCREMENT," +
				   " distance INTEGER,"+ 
				 " time INTEGER,"+
				   " calory INTEGER," +
				 " date DATE);";
		  db.execSQL(sql);
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE SAVE_IMG");
		onCreate(db);
	}
}