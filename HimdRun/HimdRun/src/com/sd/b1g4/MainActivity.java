//      ************이름은 main 이지만, 실질적으로 하는일은 splash 클래스 부르고 끝난다.**************

package com.sd.b1g4;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;

public class MainActivity extends Activity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, SplashActivity.class));    //splash 클래스로 넘어가기
        finish();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.start);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    

}
