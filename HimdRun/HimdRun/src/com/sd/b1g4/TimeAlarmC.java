//             **********시간으로 알람설정하는 부분 *****************
//             설정다하고 -> AlarmSetting
package com.sd.b1g4;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.*;
import android.view.*;
import android.content.*;


/**
 * @author 김윤경
 * @since 2013.11.18
 * @version 4.0
 * 
 * @ class TimeAlarmC
 *
 */

public class TimeAlarmC extends Activity {
	
	
	Handler mHandler;
	int tmp=0;
	
	/**
	 * 입력한 시간, 분, 초  
	 */
	String hourSet, minSet, secSet;
	int hour, min, sec;
	static int time =10000;
	MapActivity map = new MapActivity();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timeset);		
		
		
		/**
		 * 저장하기 버튼 누르면
		 */
		Button btn = (Button)findViewById(R.id.saveBtn11);  
		btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(TimeAlarmC.this, StartActivity.class);	
				startActivity(intent);
				finish();	
				
				/**
				 * 시간 저장
				 */
				EditText hourID = (EditText)findViewById(R.id.hour);
				hourSet = hourID.getText().toString();
				hour = Integer.parseInt(hourSet);
				
				/**
				 * 분 저장
				 */
				EditText minID = (EditText)findViewById(R.id.minute);
				minSet = minID.getText().toString();
				min = Integer.parseInt(minSet);
				
				/**
				 * 초 저장
				 */
				EditText secID = (EditText)findViewById(R.id.second);
				secSet = secID.getText().toString();
				sec = Integer.parseInt(secSet);				
				
				/**
				 * 통합시간
				 */
				time = (hour*1440) + (min*60) + sec ; 		
				
				Toast.makeText(getApplicationContext(),time+" 초 알람설정 되었습니다.",Toast.LENGTH_SHORT).show();
		}
    });
		
}
	
	/**
	 * 저장해 놓은 통합시간 가져갈 수 있게끔 만든 get함수
	 * 
	 * @return 통합시간(time)
	 */
	public int getTime() {
		return time;
	}
	
	
	
	 /**
	  * **운동시작때부터 시간계산해주는함수**
	  * 1초당 도는 timer 썼으니까, 1씩 더해주면 된다.
	  */
	 public int timecontrol () {
		    //exacAlarm();
		    tmp += 1;    
		    return tmp;
	  }
	   
	 
	 /**
	  * 알람 실행하는 함수
	  * 
	  */
	 public void exacAlarm() {
		    AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(TimeAlarmC.this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(TimeAlarmC.this, 0, intent, 0);
     
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.add(Calendar.SECOND, time);  
   
			am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender); 
	 }
	    


	 /**
	  * backPressed
	  * @return AlarmSetting
	  */
	@Override 
	public void onBackPressed(){
		super.onBackPressed();
		Intent intent = new Intent(TimeAlarmC.this, AlarmSetting.class);	
		startActivity(intent);
		finish();
	}

}
