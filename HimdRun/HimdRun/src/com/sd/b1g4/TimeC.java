package com.sd.b1g4;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;


/**
 * @author Kim yoon kyoung
 * 
 * @since 13.11.15
 * @version 1.1
 * 
 * @ TimeC class
 *
 */

public class TimeC extends Activity implements Calculate {
	
	int tmp=0;

	public TimeC() {
		
	}
	
	public double calculate() {
		tmp += 1;
		
		return tmp;
	}
	

}