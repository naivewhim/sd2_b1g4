package com.sd.b1g4;


import android.widget.Toast;

//소비 칼로리는 체중 10kg 중간형태 마다 약 20%가량 증가한다.
//15분당 운동계수 => 걷기 0.9, 달리기 2.0
//운동계수  * 체중  (30분:*2, 45분:*3)
//걷기 속력 -> 분당 80m
//if(거리m/시간) <=80  ==>걷기
//else ==> 달리기
//몸무게 45~55 기본 , 55~65 *1.2, 65~75 *1.4, 77~ *1.6

/**
 * @author 김윤경
 * @since 2013.11.28
 * @version 1.0
 * 
 *  CalorieC class
 *
 */
public class CalorieC extends UserInfoM{
	
	double calorie =0;
	float weight=0;
	double speed =0;
	double rate =0;
	
	DistanceC distancec = new DistanceC();
	TimeC timec = new TimeC();
	
	double dist = distancec.getDist();
	int time = timec.getTime();
	
	public CalorieC () {
		this.weight = super.weight;
	}

	public double result(){
		
		speed = dist/time;
		rate = time % 15;
		
		//걷기
		if(speed < 80) {
			calorie = weight * 0.9 * rate;
			
			if(weight > 55 && weight <= 65) {
				calorie *= 1.2;
			}
			
			else if(weight > 65 && weight <= 75) {
				calorie *= 1.4;
			}
			
			else if(weight > 76) {
				calorie *= 1.6;
			}
		}
		
		//달리기
		else {
			calorie = weight * 2.0 * rate;
			
			if(weight > 55 && weight <= 65) {
				calorie *= 1.2;
			}
			
			else if(weight > 65 && weight <= 75) {
				calorie *= 1.4;
			}
			
			else if(weight > 76) {
				calorie *= 1.6;
			}
		}
		

		return calorie;
	}
	
	public void update() {
		
	}

}