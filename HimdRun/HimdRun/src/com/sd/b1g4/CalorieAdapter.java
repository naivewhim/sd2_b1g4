package com.sd.b1g4;


/**
 * @author ������
 * @since 2013.11.26
 * @version 1.0
 *
 * CalorieAdatper class
 */
public class CalorieAdapter implements Calculate {

	CalorieC calorie;
	
	public CalorieAdapter(CalorieC calorie) {
		this.calorie = calorie;
	}
	
	public double calculate() {
	
		return calorie.result();
	}
}
