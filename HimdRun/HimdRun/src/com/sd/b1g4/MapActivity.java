package com.sd.b1g4;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.Calendar;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.TextView;
import java.text.*;


public class MapActivity extends FragmentActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener   { 
	
	private GoogleMap mMap;
	ArrayList<LatLng> points;

	  boolean bFirst = true;
	  private LocationClient mLocationClient;
	  //private TextView mMessageView;
	  
	  
	  	/**시간계산변수**/
		int tmp=0;
		double tmp2=0;
		String temp;
		Handler mHandler;
		Handler mHandler2;
		Handler mHandler3;
		int time=0;
		int caltime =0;
		boolean swit;
		double cal =0;
		
	  // These settings are the same as the settings for the map. They will in fact give you updates at
	  // the maximal rates currently possible.
	  private static final LocationRequest REQUEST = LocationRequest.create()
	      .setInterval(5000)         // 5 seconds
	      .setFastestInterval(16)    // 16ms = 60fps
	      .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	    
	  
	  
	  /****************************Command 패턴적용하기***************************/
      AlarmRemoteControl remote;   //remote control 클래스만들어서
      AlarmRemoteControl remote2;
      
      TimeAlarmC alarm;   //젤 밑에 있는 time alarm C
      TimeCommand timeCommand;    //time Command 만들기! (alarm을 파라미터로)
      
      DistanceAlarmC alarm2;   //젤 밑에 있는 distance alarm C
      DistanceCommand distanceCommand;    //distance Command 만들기! (alarm2을 파라미터로)
      
      
      
      /***************************Adapter 패턴적용하기*****************************/
      TimeC timeControler;
      DistanceC distControler;
      CalorieAdapter calorieAdapter;
      CalorieC calorie;
      
      DecimalFormat t;
      
	  
      
      
	 TextView MyText ;
	 TextView MyText2 ;
	 TextView MyText3;
	 int time2;
	 int dist2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
      
        remote = new AlarmRemoteControl();   //remote control 클래스만들어서
        remote2 = new AlarmRemoteControl();   //remote control 클래스만들어서
        
        alarm = new TimeAlarmC();   //젤 밑에 있는 time alarm C
        timeCommand = new TimeCommand(alarm);    //time Command 만들기! (alarm을 파라미터로)
        remote.setCommand(timeCommand);      //remote에 등록하기
        
        alarm2 = new DistanceAlarmC();  //젤 밑에 있는 distance alarm C
        distanceCommand = new DistanceCommand(alarm2);   //distance Command 만들기! (alarm2을 파라미터로)
        remote2.setCommand(distanceCommand);
        
        timeControler = new TimeC();
        distControler = new DistanceC();
        calorie = new CalorieC();
        calorieAdapter = new CalorieAdapter(calorie);
        
        t = new DecimalFormat("###.#");
        
        timecontrol();
        distancecontrol();
        caloriecontrol();

        
        time2=alarm.getTime();  
        setTime(time2);
        
        dist2=alarm2.getDistance();
        setDistance(dist2);
        
        MyText = (TextView) findViewById(R.id.textView1);
        MyText2 = (TextView) findViewById(R.id.textView2);
        MyText3 = (TextView) findViewById(R.id.textView4);
        
        StartActivity switcheck = new StartActivity();
        swit = switcheck.getSwit();

        
        
        /**
         * **운동종료누르면****
         * */
        Button btn = (Button)findViewById(R.id.endButton);
        btn.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {

        		Intent intent = new Intent(MapActivity.this, StartActivity.class);	
        		startActivity(intent);
        		finish();
			}
        });
        

        
       
        
        points = new ArrayList<LatLng>();
        
        // Getting reference to the SupportMapFragment of activity_main.xml
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
 
        // Getting GoogleMap object from the fragment
        mMap = fm.getMap();
 
        // Enabling MyLocation Layer of Google Map
        mMap.setMyLocationEnabled(true);
 
        // Setting OnClick event listener for the Google Map
        mMap.setOnMapClickListener(new OnMapClickListener() {
 
            @Override
            public void onMapClick(LatLng point) {
 
                // Instantiating the class MarkerOptions to plot marker on the map
                MarkerOptions markerOptions = new MarkerOptions();
 
                // Setting latitude and longitude of the marker position
                markerOptions.position(point);
 
                // Setting titile of the infowindow of the marker
                markerOptions.title("Position");
 
                // Setting the content of the infowindow of the marker
                markerOptions.snippet("Latitude:"+point.latitude+","+"Longitude:"+point.longitude);
 
                // Instantiating the class PolylineOptions to plot polyline in the map
                PolylineOptions polylineOptions = new PolylineOptions();
 
                // Setting the color of the polyline
                polylineOptions.color(Color.RED);
 
                // Setting the width of the polyline
                polylineOptions.width(3);
 
                // Adding the taped point to the ArrayList
                points.add(point);
 
                // Setting points of polyline
                polylineOptions.addAll(points);
 
                // Adding the polyline to the map
                mMap.addPolyline(polylineOptions);
 
                // Adding the marker to the map
                mMap.addMarker(markerOptions);
 
            }
        });
 
        mMap.setOnMapLongClickListener(new OnMapLongClickListener() {
 
            @Override
            public void onMapLongClick(LatLng point) {
                // Clearing the markers and polylines in the google map
            	mMap.clear();
 
                // Empty the array list
                points.clear();
            }
        });

    }
    
    
    public void setTime(int time) {
    	this.time = time;
    }
    
    public void setDistance(int dist) {
    	this.dist2 = dist;
    	Toast.makeText(getApplicationContext(),""+dist2,Toast.LENGTH_SHORT).show(); 
    }
    
    
    /********************************운동시작때부터 시간계산해주는함수**********************/
    public void timecontrol () {
    	mHandler = new Handler() { 
	    	public void handleMessage(Message msg) { 		
	    		tmp = (int)timeControler.calculate();   //계산시작하는거야 
	    		//초
	    		if(tmp<60) {
	    			temp = Integer.toString(tmp);
	    			if(tmp<10)
	    				MyText.setText("00 : 00 : 0"+ temp); 
	    			else
	    				MyText.setText("00 : 00 : "+ temp); 
	    		}
	    		
	    		//분
	    		else if(tmp>=60 && tmp<3600) {
	    			int min = tmp/60;
	    			int tmpsec = tmp%60;
	    			
	    			if(min<10 && tmp<10)
	    				MyText.setText("00 : 0"+min + " : 0" + tmpsec); 
	    			else if(min<10 )
	    				MyText.setText("00 : 0"+min + " : " + tmpsec); 
	    			else
	    				MyText.setText("00 : "+min + " : " + tmpsec); 
	    		}
	    		
	    		//시
	    		else if(tmp>3600) {
	    			int hour = tmp/3600;
	    			int min = (tmp%3600)/60;
	    			int tmpsec = (tmp%3600)%60;
	    			
	    			if(hour<10)	
	    				MyText.setText(hour+" : "+min + " : " + tmpsec); 
	    			else
	    				MyText.setText(hour+" : "+min + " : " + tmpsec); 
	    		}
	    		
	    			//설정해놓은 시간과 같아지면
	    			if(tmp == time) {
	    				exacAlarm();
	    			}

	    		mHandler.sendEmptyMessageDelayed(0, 1000);
	    	}
	    };
	    
	    mHandler.sendEmptyMessage(0);
    }
    /********************************운동시작때부터 시간계산해주는함수**********************/
    
    
    
    
	 /********************************운동시작때부터 거리계산해주는함수*************************/
    int i=0;
	 public void distancecontrol () {
	    	mHandler2 = new Handler() { 
		    	public void handleMessage(Message msg) { 		
		    		tmp2 = distControler.calculate();   //계산시작하는거야 
		    		MyText2.setText("" + tmp2); 

		    		//설정해놓은 시간과 같아지면
		    		if(tmp2 >= dist2 && i==0) {
		    		    exacAlarm();
		    		    i++;
		    		}
		    		mHandler2.sendEmptyMessageDelayed(0, 1000);
		    	}
		    };
		    mHandler2.sendEmptyMessage(0);
	 }
	 
	 /********************************운동시작때부터 거리계산해주는함수*************************/
	 
	 
    /********************************칼로리 계산된거 불러서 출력하기*************************/
    public void caloriecontrol() {
    	mHandler3 = new Handler() { 
	    	public void handleMessage(Message msg) {
	    		cal = calorieAdapter.calculate();
	    		
	    		MyText3.setText(t.format(cal) + "Kcal"); 
	    		mHandler3.sendEmptyMessageDelayed(0, 1000);
	    	}
	    };
	    
	    mHandler3.sendEmptyMessage(0);
    }
    /********************************칼로리 계산된거 불러서 출력하기*************************/
    
    
    
	 /**
	  * 알람 실행하는 함수
	  * 
	  */
	 public void exacAlarm() {
		    AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(MapActivity.this, AlarmReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(MapActivity.this, 0, intent, 0);
   
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			if(time> dist2)
				calendar.add(Calendar.SECOND, 0);  
			else if(time < dist2)
				calendar.add(Calendar.SECOND, 0);  
 
			am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender); 
	 }
   
    
    
    
    
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    

    @Override
    protected void onResume() {
      super.onResume();
      setUpMapIfNeeded();
      setUpLocationClientIfNeeded();
      mLocationClient.connect();
      goCurrentLocation();	
    }

    @Override
    public void onPause() {
      super.onPause();
      if (mLocationClient != null) {
        mLocationClient.disconnect();
      }
    }


    public synchronized void goCurrentLocation(){
  	/*Location lcCurrent = mMap.getMyLocation();
  	if(lcCurrent!=null){
  		mMap.getProjection().toScreenLocation(new LatLng(lcCurrent.getLatitude(), lcCurrent.getLongitude()));
  	}*/
  	  //mMap.getProjection().toScreenLocation(new LatLng(36.5, 102.5));
  	  
    }
    
    private void setUpMapIfNeeded() {
      // Do a null check to confirm that we have not already instantiated the map.
      if (mMap == null) {
        // Try to obtain the map from the SupportMapFragment.
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
               .getMap();
        // Check if we were successful in obtaining the map.
        if (mMap != null) {
          mMap.setMyLocationEnabled(true);	
        }
      }
    }

    private void setUpLocationClientIfNeeded() {
      if (mLocationClient == null) {
        mLocationClient = new LocationClient(
            getApplicationContext(),
            this,  // ConnectionCallbacks
            this); // OnConnectionFailedListener
      }
    }

    /**
     * Button to get current Location. This demonstrates how to get the current Location as required,
     * without needing to register a LocationListener.
     */
    public void showMyLocation(View view) {
      if (mLocationClient != null && mLocationClient.isConnected()) {
        String msg = "Location = " + mLocationClient.getLastLocation();
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
      }
    }

    /**
     * Implementation of {@link LocationListener}.
     */
    @Override
    public void onLocationChanged(Location location) {
  	  if(location!=null){
  		  if(bFirst==false)return;
  		  //mMessageView.setText("Location = " + location);
  		  
  		  double latitude = location.getLatitude();
  		  double longitude = location.getLongitude();
  		  //Log.d(TAG, "GPS Position [" + String.valueOf(latitude) + "," + String.valueOf(longitude) + "]") ;
  		  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16));
  		  bFirst = false;
  	  }
    }

    /**
     * Callback called when connected to GCore. Implementation of {@link ConnectionCallbacks}.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
      mLocationClient.requestLocationUpdates(
          REQUEST,
          this);  // LocationListener
    }

    /**
     * Callback called when disconnected from GCore. Implementation of {@link ConnectionCallbacks}.
     */
    @Override
    public void onDisconnected() {
      // Do nothing
    }

    /**
     * Implementation of {@link OnConnectionFailedListener}.
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
      // Do nothing
    }
    
    @Override
	public void onBackPressed(){
		super.onBackPressed();
		
		Intent intent = new Intent(MapActivity.this, StartActivity.class);
		
		startActivity(intent);
		finish();
	}
  }