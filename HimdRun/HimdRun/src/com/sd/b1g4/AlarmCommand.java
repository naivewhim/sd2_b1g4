package com.sd.b1g4;

/**
 * 
 * @author Kim yoon kyoung
 * @version 1.0
 * @since 13.11.15
 * 
 * @ AlarmCommand class
 * 
 */

public interface AlarmCommand {
	
	public int execute();
	public void execAlarm();

}
